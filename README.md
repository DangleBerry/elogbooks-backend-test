### ElogBooks Backend Coding Test

## Installation
The application uses docker for its infrastructure. Please follow the instructions below to install 
and configure. 

1. `cp .env.dist .env` Execute this command upon cloning from Gitlab.
1. `cp .env.test.dist .env.test` Same as above.
1. `docker-compose build --no-cache` This command should only be used if changes are made to the Dockerfile or for first time installation.
1. `docker-compose up -d` This will bring the application and accessible via the web http://localho.st URL.

## Configuration
After installation and if the database doesn't exist with the application schema and data then execute
the following commands to migrate to the latest schema and insert some dummy fixtures. 

Please be aware that executing the doctrine scripts before MySQL services are loaded could result in a connection refused message.

1. `docker exec elogbooks-webserver bin/console doctrine:migrations:migrate -n` This command loads the schema.
1. `docker exec elogbooks-webserver bin/console doctrine:fixtures:load -n` This loads some test data

## Testing
When application is fully installed and configured the following will configure the units to run. 
1. `docker exec elogbooks-webserver bin/console --env=test doctrine:migrations:migrate -n`
1. `docker exec elogbooks-webserver bin/console --env=test doctrine:fixtures:load -n`
1. `docker exec elogbooks-webserver vendor/bin/phpunit`
