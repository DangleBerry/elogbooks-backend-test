<?php

namespace App\Tests\Controller;

use App\Entity\Job;
use App\Entity\Property;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class JobControllerTest extends WebTestCase
{
    public function testJobCreateSuccess(): Job
    {
        $client = static::createClient();
        $job = $this->dataProvider();

        $data = [
            'summary'       => $job->getSummary(),
            'description'   => $job->getDescription(),
            'status'        => $job->getStatus(),
            'property'      => [
                'name'      => $job->getProperty()->getName()
            ],
            'raisedBy'      => [
                'firstname' => $job->getRaisedBy()->getFirstname(),
                'surname'   => $job->getRaisedBy()->getSurname()
            ]
        ];
        $client->request(
            'POST',
            '/api/job',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json'
            ],
            json_encode($data)
        );

        $content = json_decode($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_CREATED, $client->getResponse()->getStatusCode());
        $this->assertObjectHasAttribute('id', $content);

        $job->setId($content->id);

        return $job;
    }

    public function testJobCreateBadRequestException()
    {
        $client = static::createClient();
        $client->request(
            'POST',
            '/api/job',
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json'
            ],
            '{}'
        );

        $content = json_decode($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
        $this->assertObjectHasAttribute('errors', $content);
        $this->assertIsArray($content->errors);
        $this->assertTrue(count($content->errors) > 0);
    }

    /**
     * @depends testJobCreateSuccess
     */
    public function testJobUpdateSuccess(Job $job)
    {
        $client = static::createClient();
        $data = [
            'id'          => $job->getId(),
            'summary'     => 'Wembley stadium needs a clean',
            'description' => 'All the seats at the stadium needs cleaning and toilets',
            'status'      => 'in progress',
            'property'    => [
                'name'  => 'Wembley Stadium'
            ],
            'raisedBy'    => [
                'firstname' => 'Harry',
                'surname'   => 'Kane'
            ]
        ];

        $client->request(
            'PUT',
            '/api/job/' . $job->getId(),
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json'
            ],
            json_encode($data)
        );

        $content = json_decode($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_OK, $client->getResponse()->getStatusCode());
        $this->assertObjectHasAttribute('id', $content);
    }

    /**
     * @depends testJobCreateSuccess
     */
    public function testJobUpdateBadRequest(Job $job)
    {
        $client = static::createClient();
        $client->request(
            'PUT',
            '/api/job/' . $job->getId(),
            [],
            [],
            [
                'CONTENT_TYPE' => 'application/json'
            ],
            '{}'
        );

        $content = json_decode($client->getResponse()->getContent());
        $this->assertEquals(Response::HTTP_BAD_REQUEST, $client->getResponse()->getStatusCode());
        $this->assertObjectHasAttribute('errors', $content);
        $this->assertIsArray($content->errors);
        $this->assertTrue(count($content->errors) > 0);
    }

    public function testJobListSuccess()
    {
        $client = static::createClient();
        $client->request(
            'GET',
            '/api/job'
        );

        $responseContent = json_decode($client->getResponse()->getContent());
        $this->assertResponseIsSuccessful();
        $this->assertIsArray($responseContent);
        $this->assertTrue(count($responseContent) > 0);
    }

    /**
     * @depends testJobCreateSuccess
     */
    public function testJobIndexSuccess(Job $job)
    {
        $client = static::createClient();
        $client->request(
            'GET',
            '/api/job/' . $job->getId()
        );

        $this->assertResponseIsSuccessful();
        $content = json_decode($client->getResponse()->getContent());
        $this->assertObjectHasAttribute('id', $content);
    }

    /**
     * @depends testJobCreateSuccess
     */
    public function testJobDeleteSuccess(Job $job): int
    {
        $client = static::createClient();
        $client->request(
            'DELETE',
            '/api/job/' . $job->getId()
        );

        $this->assertResponseIsSuccessful();
        $content = json_decode($client->getResponse()->getContent());
        $this->assertObjectHasAttribute('status', $content);
        $this->assertEquals('success', $content->status);

        return $job->getId();
    }

    /**
     * @depends testJobDeleteSuccess
     */
    public function testJobDeletePageNotFound(int $jobId)
    {
        $this->expectException(NotFoundHttpException::class);
        $client = static::createClient();
        $client->catchExceptions(false);
        $client->request(
            'DELETE',
            '/api/job/' . $jobId
        );
    }

    public function testJobIndexPageNotFound()
    {
        $this->expectException(NotFoundHttpException::class);
        $client = static::createClient();
        $client->catchExceptions(false);
        $client->request(
            'GET',
            '/api/job/' . PHP_INT_MAX
        );
    }

    public function testJobUpdatePageNotFound()
    {
        $this->expectException(NotFoundHttpException::class);
        $client = static::createClient();
        $client->catchExceptions(false);
        $client->request(
            'PUT',
            '/api/job/' . PHP_INT_MAX,
            []
        );
    }

    private function dataProvider(): Job
    {
        $property = new Property();
        $property->setName('Kremlin');

        $user = new User();
        $user->setFirstname('Raheem');
        $user->setSurname('Sterling');

        $job = new Job();
        $job->setSummary('Leaking drain pipe');
        $job->setDescription('Drain pipe out side the bedroom of the president is making him a sleepless nights');
        $job->setStatus('open');
        $job->setRaisedBy($user);
        $job->setProperty($property);

        return $job;
    }
}
