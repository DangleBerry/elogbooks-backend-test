<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Exception;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 */
class Job
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=150)
     * @Assert\NotBlank(message = "Summary is required")
     * @Assert\Length(
     *     min = 1,
     *     max = 150,
     *     minMessage = "Summary must not be blank",
     *     maxMessage = "Summary can not be longer than 150 characters long"
     * )
     */
    private string $summary;

    /**
     * @ORM\Column(type="string", length=500)
     * @Assert\NotBlank(message = "Description is required")
     * @Assert\Length(
     *     min = 1,
     *     max = 500,
     *     minMessage = "Description must not be blank",
     *     maxMessage = "Description can not be longer than 500 characters long"
     * )
     */
    private string $description;

    /**
     * @ORM\Column(type="string", columnDefinition="ENUM('open', 'in progress', 'completed', 'cancelled')")
     * @Assert\NotBlank(message = "Status is required")
     * @Assert\Choice({"open", "in progress", "completed", "cancelled"}, message = "Invlalid status")
     */
    private string $status;

    /**
     * @ORM\OneToOne(targetEntity="Property", cascade={"persist"})
     * @ORM\JoinColumn(name="property_id", referencedColumnName="id")
     */
    private Property $property;

    /**
     * @ORM\OneToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private User $raisedBy;

    public function __construct()
    {
        $this->status = 'open';
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setSummary(string $summary): self
    {
        $this->summary = $summary;
        return $this;
    }

    public function getSummary(): string
    {
        return $this->summary;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;
        return $this;
    }

    public function setProperty(Property $property): self
    {
        $this->property = $property;
        return $this;
    }

    public function getProperty(): Property
    {
        return $this->property;
    }

    public function setRaisedBy(User $user): self
    {
        $this->raisedBy = $user;
        return $this;
    }

    public function getRaisedBy(): User
    {
        return $this->raisedBy;
    }
}
