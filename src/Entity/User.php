<?php


namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity
 * Class User
 * @package App\Entity
 */
class User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank(message= "Firstname is required")
     * @Assert\Length(
     *   max = 32,
     *   maxMessage = "First name can not be longer than 32 characters long"
     * )
     * @var string
     */
    private string $firstname;

    /**
     * @ORM\Column(type="string", length=32)
     * @Assert\NotBlank(message = "Surname can not be blank")
     * @Assert\Length(
     *    max = 32,
     *    maxMessage = "Surname can not be longer than 32 characters long"
     * )
     */
    private string $surname;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function setFirstname(string $firstname): self
    {
        $this->firstname = $firstname;
        return $this;
    }

    public function getSurname(): string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;
        return $this;
    }
}
