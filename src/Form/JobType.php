<?php

namespace App\Form;

use App\Entity\Job;
use App\Entity\Property;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class JobType extends \Symfony\Component\Form\AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id', HiddenType::class, [
                'empty_data' => '0'
            ])
            ->add('summary', TextType::class, [
                'empty_data' => ''
            ])
            ->add('description', TextType::class, [
                'empty_data' => ''
            ])
            ->add('status', TextType::class, [
                'empty_data' => 'open'
            ])
            ->add('property', PropertyType::class)
            ->add('raisedBy', UserType::class);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Job::class,
        ]);
    }
}
