<?php

namespace App\DataFixtures;

use App\Entity\Job;
use App\Entity\Property;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class JobFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $property = new Property();
        $property->setName('Buckingham Palace');

        $user = new User();
        $user->setFirstname('Matthew');
        $user->setSurname('Cossins');

        $job = new Job();
        $job->setSummary('Repair toilet');
        $job->setDescription('Repair downstairs toilet');
        $job->setStatus('open');
        $job->setProperty($property);
        $job->setRaisedBy($user);

        $manager->persist($job);
        $manager->flush();
    }
}
