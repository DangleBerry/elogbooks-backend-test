<?php


namespace App\Controller;

use App\Entity\Job;
use App\Form\JobType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class JobController extends \Symfony\Bundle\FrameworkBundle\Controller\AbstractController
{
    private SerializerInterface $serializer;

    /**
     * JobController constructor.
     * @param SerializerInterface $serializer
     */
    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/api/job", methods={"GET"})
     * @return JsonResponse
     */
    public function list(): JsonResponse
    {
        $data = $this->getDoctrine()->getRepository(Job::class)->findAll();

        return new JsonResponse($this->serializer->serialize($data, 'json'), 200, [], true);
    }

    /**
     * Delete job
     *
     * @Route("/api/job/{id}", methods={"DELETE"})
     * @param int $id
     * @return JsonResponse
     */
    public function delete(int $id): JsonResponse
    {
        $job = $this->getDoctrine()->getRepository(Job::class)->find($id);
        if (empty($job)) {
            throw $this->createNotFoundException();
        }

        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($job);
        $entityManager->flush();

        return new JsonResponse([
            'status' => 'success'
        ]);
    }

    /**
     * @Route("/api/job/{id}", methods={"PUT"})
     * @param int $id
     * @param Request $request
     * @return JsonResponse
     */
    public function update(int $id, Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $job = $this->getDoctrine()->getRepository(Job::class)->find($id);
        if (empty($job)) {
            throw $this->createNotFoundException();
        }

        $form = $this->createForm(JobType::class, $job);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->flush();
        } else {
            return $this->createBadRequestResponse($this->processFormErrors($form));
        }

        return new JsonResponse(
            $this->serializer->serialize($job, 'json'),
            Response::HTTP_OK,
            [],
            true
        );
    }

    /**
     * Create a new job
     *
     * @Route("/api/job", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function create(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $job = new Job();
        $form = $this->createForm(JobType::class, $job);
        $form->submit($data);
        if ($form->isSubmitted() && $form->isValid()) {
            $job = $form->getData();
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($job);
            $entityManager->flush();
        } else {
            return $this->createBadRequestResponse($this->processFormErrors($form));
        }

        return new JsonResponse(
            $this->serializer->serialize($job, 'json'),
            Response::HTTP_CREATED,
            [],
            true
        );
    }

    /**
     * Return job information
     *
     * @Route("/api/job/{id}", methods={"GET"})
     * @param int $id
     * @return JsonResponse
     */
    public function index(int $id): JsonResponse
    {
        $job = $this->getDoctrine()->getManager()->getRepository(Job::class)->find($id);
        if (empty($job)) {
            throw $this->createNotFoundException();
        }

        return new JsonResponse($this->serializer->serialize($job, 'json'), 200, [], true);
    }

  /**
   * @Route("/api/job{pattern}", methods={"OPTIONS"}, requirements={"pattern"="(?:\/||\/\d+)"})
   * @return JsonResponse
   */
    public function options(): JsonResponse
    {
      return new JsonResponse([
          'status' => 'success'
      ]);
    }

  /**
   * Return BadRequestResponse
   *
   * @param array $data
   * @return JsonResponse
   */
    private function createBadRequestResponse(array $data = []): JsonResponse
    {
        return new JsonResponse($data, 400);
    }

    /**
     * Custom function to process form errors into array to be returned to user
     *
     * @param FormInterface $form
     * @return array
     */
    private function processFormErrors(FormInterface $form): array
    {
        $errors['errors'] = [];
        foreach ($form->getErrors(true) as $error) {
            $errors['errors'][] = $error->getMessage();
        }
        return $errors;
    }
}
