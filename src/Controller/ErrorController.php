<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;

class ErrorController
{
    public function __invoke(\Throwable $exception): JsonResponse
    {
        $data = [
          'message' => $exception->getMessage()
        ];

        if ($exception instanceof HttpException) {
          $data['statusCode'] = $exception->getStatusCode();
        }

        return new JsonResponse($data);
    }
}
